import React from "react"
import {render} from "react-dom"
import App from "./components/App"
import PageNotFound from "./components/PageNotFound"
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';

window.React = React

render(
    <Router>
        <div>
            <Switch>
                <Route path="/" exact component={App}/>
                <Route path="/add" component={App} />
                <Route path="/surf-listing" component={App} />
                <Route component={PageNotFound} />
            </Switch>
        </div>
    </Router>,
    document.getElementById('react-container')
)
