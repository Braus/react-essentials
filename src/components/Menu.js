import { Link } from "react-router-dom"
import HomeIcon from "react-icons/lib/fa/home"
import AddDayIcon from "react-icons/lib/fa/calendar-plus-o"
import ListDayIcons from "react-icons/lib/fa/table"

const Menu = () => {
    return (
        <nav className="menu">
            <Link to="/"><HomeIcon /></Link>
            <Link to="/add"><AddDayIcon /></Link>
            <Link to="/surf-listing"><ListDayIcons /></Link>
        </nav>
    )
}

export default Menu
