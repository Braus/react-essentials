import createClass from "create-react-class"
import SurfDayList from "./SurfDayList"
import SurfDayCount from "./SurfDayCount"
import AddDayForm from "./AddDayForm"
import PageNotFound from "./PageNotFound"
import Menu from "./Menu"
import { Component } from "react"

class App extends Component {
    constructor(props){
        super(props)
        this.state = {
            allSurfDays:
            [
                {
                    beach: "Snapper Rocks",
                    date: new Date("1/2/2016"),
                    goodDay: true,
                    badDay: false
                },
                {
                    beach: "Miami Beach",
                    date: new Date("1/4/2016"),
                    goodDay: false,
                    badDay: true
                },
                {
                    beach: "D'bah",
                    date: new Date("1/5 /2016"),
                    goodDay: true,
                    badDay: false
                }
            ]
        }
    }

    countDays(filter){
            return this.state.allSurfDays.filter(
                (day) => filter ? day[filter] : day
            ).length
    }

    render(){
        return(
            <div className="app">
                <Menu />
                {
                    (this.props.location.pathname === "/") ?
                        <SurfDayCount total={this.countDays()}
                            goodDay={this.countDays('goodDay')}
                            badDay={this.countDays('badDay')}/>
                    : (this.props.location.pathname === "/add") ?
                        <AddDayForm />
                    :  (this.props.location.pathname === "/surf-listing") ?
                        <SurfDayList days={this.state.allSurfDays} />
                    : <PageNotFound />
                }
            </div>
        )
    }
}

export default App
