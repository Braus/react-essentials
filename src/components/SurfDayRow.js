import Terrain from "react-icons/lib/md/terrain"
import SnowFlake from "react-icons/lib/ti/weather-snow"
import Calendar from "react-icons/lib/fa/calendar"
// import { PropTypes } from "react"
import { PropTypes } from "prop-types"

const SurfDayRow = ({beach, date, goodDay, badDay}) => (
    <tr>
        <td>
            {date.getMonth()+1}/{date.getDate()}/
			{date.getFullYear()}
        </td>
        <td>{ beach }</td>
        <td>{(goodDay) ? <SnowFlake /> : null}</td>
        <td>{(badDay) ? <Terrain /> : null}</td>
    </tr>
)

SurfDayRow.propTypes = {
    beach: PropTypes.string.isRequired,
    date: PropTypes.instanceOf(Date).isRequired,
    goodDay: PropTypes.bool,
    badDay: PropTypes.bool
}


export default SurfDayRow
