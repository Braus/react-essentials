import Terrain from "react-icons/lib/md/terrain"
import SnowFlake from "react-icons/lib/ti/weather-snow"
import Calendar from "react-icons/lib/fa/calendar"

import SurfDayRow from "./SurfDayRow"

import { PropTypes } from "prop-types"

const SurfDayList = ({days}) => (
    <div className="surf-day-list">
        <table>
            <thead>
                <tr>
                    <th>Date</th>
                    <th>Beach</th>
                    <th>Good Day</th>
                    <th>Bad Day</th>
                </tr>
            </thead>
            <tbody>
                {days.map((day, i) =>
                    <SurfDayRow key={i}
                        {...day }/>
                )}
            </tbody>
        </table>
    </div>
)

SurfDayList.propTypes ={
    days: function(props){
        if(!Array.isArray(props.days)){
            return new Error("Surf Day List must be an array")
        } else if(!props.days.length){
            return new Error("Surf Day List is an empty array")
        } else {
            return null
        }
    }
}

export default SurfDayList
