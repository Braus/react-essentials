import "../stylesheets/ui.scss"
import Terrain from "react-icons/lib/md/terrain"
import SnowFlake from "react-icons/lib/ti/weather-snow"
import Calendar from "react-icons/lib/fa/calendar"
// import { PropTypes } from "react"
import { PropTypes } from "prop-types"


const percentToDecimal = (decimal) => {
    return ((decimal * 100) + "%")
}

const calcGoalProgress = (total, goal) => {
    return percentToDecimal(total / goal)
}

const SurfDayCount = ({goal, total, goodDay, badDay}) => (

    <div className="surf-day-count">
        <div className="goal">
            <span>Goal: {goal} days</span>
        </div>
        <div className="total-days">
            <span><Calendar /> {total} days surfed</span>
        </div>
        <div className="good-days">
            <span><Terrain />{goodDay} good days</span>
        </div>
        <div className="bad-days">
            <span><SnowFlake /> {badDay} bad days</span>
        </div>
        <div>
            <span>My goal percentage: {calcGoalProgress(total, goal)}</span>
        </div>
    </div>

)

SurfDayCount.defaultProps = {
    goal:100,
    total: 50,
    goodDay: 20,
    badDay: 10
}

SurfDayCount.propTypes = {
    goal: PropTypes.number,
    total: PropTypes.number,
    goodDay: PropTypes.number,
    badDay: PropTypes.number
}


export default SurfDayCount
